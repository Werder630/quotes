package apps.werder.net.quotes;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;

import apps.werder.engapps.quotes.R;


public class MainActivity extends ActionBarActivity implements View.OnTouchListener {
    Cursor cursor;
    SQLiteDatabase db;

    View card;
    TextView position;
    TextView orig;
    TextView trans;
    TextView autor;

    int card_w;
    int w, h;
    int margin_w, margin_h;
    View sv_top, sv_bottom;
    float startX, stopX;

    int currentCount;
    String language;
    int id_author;
    Menu menu;

    int quantityScroll;
    SharedPreferences mSettings;

    private InterstitialAd mInterstitialAd;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        w = metrics.widthPixels;
        h = metrics.heightPixels;
        margin_w = (int) Math.round(((double) w * 0.05));
        margin_h = (int) Math.round(((double) h * 0.03));
        card_w = (w - (margin_w * 0x2));
        card = findViewById(R.id.card_view);
        card.setOnTouchListener(this);
        sv_top = findViewById(R.id.sv_top);
        sv_top.setOnTouchListener(this);
        sv_bottom = findViewById(R.id.sv_bottom);
        sv_bottom.setOnTouchListener(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, -1);
        params.setMargins(margin_w, margin_h, margin_w, margin_h);
        card.setLayoutParams(params);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowHomeEnabled(true);
        ab.setIcon(R.mipmap.ic_launcher);
        ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#385AA9")));

        position = (TextView) findViewById(R.id.currentnumber);
        orig = (TextView) findViewById(R.id.original);
        trans = (TextView) findViewById(R.id.translate);
        autor = (TextView) findViewById(R.id.autor);

        position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMoveToPosDialog();
            }
        });

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7111132956833864/1603003835");
        requestNewInterstitial();
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });
        updateCard();

        mSettings = getSharedPreferences("settings", MODE_PRIVATE);

        if (!mSettings.getBoolean("isShowedExplainDialog", false))
        {getExplainDialog();}
    }

    private void getExplainDialog() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Внимание");
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.stud_explain_dialog, null);
        adb.setView(view);

        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor e = mSettings.edit();
                e.putBoolean("isShowedExplainDialog", true);
                e.commit();
                dialog.cancel();

            }});

        AlertDialog ad = adb.create();
        ad.show();

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        language = savedInstanceState.getString("currentLanguage");
        id_author = savedInstanceState.getInt("currentAuthor", 0);
        updateCard(savedInstanceState.getInt("currentPosition", 1));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("currentPosition", cursor.getPosition());
        outState.putString("currentLanguage", language);
        outState.putInt("currentAuthor", id_author);
        super.onSaveInstanceState(outState);
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("YOUR_DEVICE_HASH")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    void updateCard() {
        updateCard(0);
    }

    void updateCard(int pos) {
        db = new DBHelper(this).openDataBase();
        String query = "select q.origin, q.translate, a.original, a.translate, a.lang from questions q inner join authors a on\n" + "q.id_author = a._id";
        String crossWord = "where";

        if (language != null)
            query += " "+crossWord+" a.lang = \'" + language + "\'";
            crossWord = "and";
        if (id_author > 0)
            query += " "+crossWord+" q.id_author = \'" + id_author + "\'";
        cursor = db.rawQuery(query, null);
        currentCount = cursor.getCount();
        cursor.moveToPosition(pos);
        db.close();
        updateTextFields();
    }

    void updateTextFields() {
        position.setText((cursor.getPosition() + 1) + " / " + currentCount);
        orig.setText(cursor.getString(0));
        trans.setText(cursor.getString(1));
        autor.setText(cursor.getString(2) + " / " + cursor.getString(3));
    }

    public void getMoveToPosDialog() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Переход к позиции");
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogview = inflater.inflate(R.layout.movetopos_layout, null);
        final TextView tv = (TextView) dialogview.findViewById(R.id.textView1);
        final SeekBar sb = (SeekBar) dialogview.findViewById(R.id.seekBar1);
        sb.setMax((currentCount - 1));
        sb.setProgress(cursor.getPosition());
        tv.setText((sb.getProgress() + 1) + " / " + currentCount);
        adb.setView(dialogview);
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onStopTrackingTouch(SeekBar arg0) {
            }

            public void onStartTrackingTouch(SeekBar arg0) {
            }

            public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
                tv.setText((sb.getProgress()+1)+" / " + currentCount);
            }
        });

        adb.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {
                arg0.cancel();
            }
        });

        adb.setPositiveButton("Перейти", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {
                cursor.moveToPosition(sb.getProgress());
                updateTextFields();
            }
        });
        AlertDialog ad = adb.create();
        ad.show();
    }

    public boolean onTouch(View v, MotionEvent me) {
        if ((v.getId() == R.id.card_view) || (v.getId() == R.id.sv_top) || (v.getId() == R.id.sv_bottom)) {
            Animation start;
            if (me.getAction() == 0) {
                startX = me.getX();
            }
            if (me.getAction() == 1) {
                stopX = me.getX();
                if (startX > (stopX + 10.0f)) {
                    start = createAnimationStart(-1);
                    card.startAnimation(start);
                    start.setAnimationListener(new Animation.AnimationListener() {

                        public void onAnimationStart(Animation arg0) {
                        }

                        public void onAnimationRepeat(Animation arg0) {
                        }

                        public void onAnimationEnd(Animation arg0) {
                            update_data(true);
                            card.startAnimation(createAnimationFinish(-1));
                        }
                    });
                    return true;
                }
                if (startX < (stopX - 10.0f)) {
                    start = createAnimationStart(1);
                    card.startAnimation(start);
                    start.setAnimationListener(new Animation.AnimationListener() {

                        public void onAnimationStart(Animation arg0) {
                        }

                        public void onAnimationRepeat(Animation arg0) {
                        }

                        public void onAnimationEnd(Animation arg0) {
                            update_data(false);
                            card.startAnimation(createAnimationFinish(1));
                        }
                    });
                }
            }
        }
        return true;
    }

    void update_data(boolean b) {
        if (b) {
            if (cursor.isLast()) {
                cursor.moveToFirst();
            } else {
                cursor.moveToNext();
            }
        } else {
            if (cursor.isFirst()) {
                cursor.moveToLast();
            } else {
                cursor.moveToPrevious();
            }
        }
        position.setText((cursor.getPosition() + 1) + " / " + currentCount);
        orig.setText(cursor.getString(0));
        trans.setText(cursor.getString(1));
        autor.setText(cursor.getString(2) + " / " + cursor.getString(3));

        if (quantityScroll++ == 20) {
            if (mInterstitialAd.isLoaded()) {
                Log.i("test2", "inner " + quantityScroll);
                mInterstitialAd.show();
            }
            quantityScroll = 0;
        };
    }

    AnimationSet createAnimationStart(int i) {
        AnimationSet as = new AnimationSet(true);
        TranslateAnimation ta = new TranslateAnimation(0.0f, (float) ((card_w + margin_w) * i), 0.0f, 0.0f);
        ta.setDuration(0xfa);
        as.addAnimation(ta);
        return as;
    }

    AnimationSet createAnimationFinish(int i) {
        AnimationSet as = new AnimationSet(true);
        TranslateAnimation ta = new TranslateAnimation((float) (-card_w * i), 0.0f, 0.0f, 0.0f);
        ta.setDuration(0xfa);
        as.addAnimation(ta);
        return as;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        this.menu = menu;
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.selectauthor: {
                createSelectAuthorDialog();
                menu.getItem(0).setIcon(getResources().getDrawable(R.mipmap.account_active));
                break;
            }
            case R.id.showall: {
                id_author = 0;
                updateCard();
                menu.getItem(0).setIcon(getResources().getDrawable(R.mipmap.account));
                break;
            }
            case R.id.english: {
                setLanguage("E", "EN");
                break;
            }
            case R.id.spanish: {
                setLanguage("S", "SP");
                break;
            }
            case R.id.all_language: {
                setLanguage(null, "Язык");
                break;
            }
            default: {
                break;
            }
        }
        return true;
    }

    private void setLanguage(String databaseValue, String actionBarValue) {
        id_author = 0;
        menu.getItem(1).setTitle(actionBarValue);
        menu.getItem(0).setIcon(getResources().getDrawable(R.mipmap.account));
        language = databaseValue;
        updateCard();
    }

    private void createSelectAuthorDialog() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Выберите автора");
        db = new DBHelper(this).openDataBase();
        String query = "select _id, translate from authors";
        if (language != null) {
            query += " where lang = '" + language + "\'";
        }
        final Cursor temp = db.rawQuery(query, null);
        currentCount = cursor.getCount();
        temp.moveToFirst();
        ArrayList<String> autorlist = new ArrayList<String>();

        while (!temp.isAfterLast()) {
            autorlist.add(temp.getString(temp.getColumnIndex("translate")));
            temp.moveToNext();
        }
        adb.setItems(autorlist.toArray(new CharSequence[autorlist.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                temp.moveToPosition(which);
                id_author = temp.getInt(0);
                updateCard();
            }
        });
        adb.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {
                arg0.cancel();
            }
        });

        adb.create().show();
        db.close();
    }
}
